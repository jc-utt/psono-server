[uwsgi]
#http-socket = 8080
http = :8080
chdir = /root/psono
module = wsgi
master = true
processes = {{ UWSGI_PROCESSES }}

die-on-term = true
buffer-size = {{ UWSGI_BUFFER_SIZE }}
